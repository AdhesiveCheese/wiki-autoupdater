#!/usr/bin/env python3

import praw
from prawcore.exceptions import ServerError, RequestException
from time import time, sleep
import logging
import sys
import traceback
import re

# Log to stdout
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

# Needed Variables
botConfig = "cheesebot"
subreddit = "cheesestash"
sub = praw.Reddit(botConfig).subreddit(subreddit)


def sendNoticeToModmail(id):
    # ToDo: Actually Do this
    logger.debug(f"Sent Modmail notifying of flairbot wiki page update for {id}")


def getEventPost(item):
    title = item.title.lower()
    if "meta monday" in title:
        return "metas"
    if "workshop wednesday" in title:
        return "workshops"
    if "open forum friday" in title:
        return "forums"
    if "theme sunday" in title:
        return "theme"
    return "N/A"


def cutText(text, start, end):
    start = text.find(start) + len(start)
    end = text.find("end")
    return text[start:end]


def addNewWikiLine(item):
    postText = item.selftext
    flairbotlink = re.search("flair \[(.*?)\)", postText)
    flairbotlink = f"[{flairbotlink})"
    PM_content = cutText(flairbotlink, "&message=", ")")
    flair = cutText(flairbotlink, "flair [", "]")
    wikiPage = sub.wiki["moderation/flairbot"]
    wikiContent = wikiPage.content_md
    newContent = ""
    if event == "theme":
        lineNumber = 0
        for line in wikiContent.splitlines():
            newContent += line + "\n"
            lineNumber += + 1
            if lineNumber == 5:
                newContent += f"{PM_content}|{flair}|{PM_content}|post:{item.id}||\n"
    logger.info(f"Adding {PM_content} from {item.id}")
    wikiPage.edit(content=newContent, reason=f"Auto-insert {flair} for {item.id}")
    sendNoticeToModmail(item.id)


def ReplaceWikiID(event, id):
    wikiPage = sub.wiki["moderation/flairbot"]
    targetLine = ""
    wikiContent = wikiPage.content_md
    newContent = ""
    if event == "metas":
        targetLine = "metas | Meta Shifter | metas | post:"
    if event == "workshops":
        targetLine = "workshops | Workshop Certified | workshops | post:"
    if event == "forums":
        targetLine = "forums | Senatorial Regular | forums | post:"
    for line in wikiContent.splitlines():
        if line.startswith(targetLine):
            line = line.split("|")
            newContent += f"{line[0]}|{line[1]}|{line[2]}| post:{id} ||\n"
        else:
            newContent += line + "\n"
    logger.info(f"Updating {event} to id {id}")
    wikiPage.edit(content=newContent, reason=f"Auto-update ID for {event}")
    sendNoticeToModmail(id)


def buildEventsTeam():
    events_team = []
    # Add Subreddit Mods to events team
    for moderator in sub.moderator():
        events_team.append(str(moderator).lower())
    # Append any other regular contributors
    events_team.append("moonfacedmask")
    #events_team.append("other_events_team_username")
    return events_team


def processEventsPosts(events_team, item):
    if any(author.lower() == str(item.author).lower() for author in events_team):
        event = getEventPost(item)
        if event == "theme":
            addNewWikiLine(item)
        elif event != "N/A":
            updateWiki(event, item.id)


def main():
    retry_time = 30
    events_team = buildEventsTeam()
    while True:
        try:
            for item in sub.stream.submissions(skip_existing=True):
                processEventsPosts(events_team, item)
        except ServerError as e:
            logger.error(f"Server Exception. Trying to reconnect in {retry_time} seconds")
            sleep(retry_time)
        except RequestException as e:
            logger.error(f"Request Exception. Trying to reconnect in {retry_time} seconds")
            sleep(retry_time)
        except Exception as e:
            logger.error(e, traceback.format_exc())
            sleep(retry_time)


if __name__ == "__main__":
    logger.info("EventsWikiUpdater Started.")
    main()
